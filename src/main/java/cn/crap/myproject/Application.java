package cn.crap.myproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@EnableAutoConfiguration // 有且仅有一个
@ComponentScan // Application。java如果在跟目录下，不需要任何配置，系统中的@Component，@Service，@Repository,@Controller 等都将自动注册至spring中
// @Import(value = { null }) 引入配置文件，和@ComponentScan（自动扫描所有配置）只能二选一
// @ImportResource 引入其他配置文件
// @EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class}) 禁用配置
// @SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}